// @flow

import * as React from 'react';
import { TaskList, TaskNew, TaskDetails, TaskEdit } from '../src/task-components';
import { type Task } from '../src/task-service';
import { shallow } from 'enzyme';
import { Form, Button, Column } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/task-service', () => {
  class TaskService {
    getAll() {
      return Promise.resolve([
        { id: 1, title: 'Les leksjon', done: false, description: "One" },
        { id: 2, title: 'Møt opp på forelesning', done: false, description: "Two" },
        { id: 3, title: 'Gjør øving', done: false, description: "Three" },
      ]);
    }

    create(title: string) {
      return Promise.resolve(4); // Same as: return new Promise((resolve) => resolve(4));
    }

    get(id: number) {
      return Promise.resolve(
        {id: 1, title: 'Les leksjon', done: false, description: "One"}
      )
    }
    uppdate(id: Number, title: string) {
      return Promise.resolve(
      {id: 1, title: 'New title', done: false, description: "New description"}
      )
    }
    delete(id: number) {
      return Promise.resolve(
        {}
      );
    }
  }
  return new TaskService();
});


describe('Task component tests', () => {
  test('TaskList draws correctly', (done) => {
    const wrapper = shallow(<TaskList />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <NavLink to="/tasks/1">Les leksjon</NavLink>,
          <NavLink to="/tasks/2">Møt opp på forelesning</NavLink>,
          <NavLink to="/tasks/3">Gjør øving</NavLink>,
        ])
      ).toEqual(true);
      done();
    });
  });

  test('TaskNew correctly sets location on create', (done) => {
    const wrapper = shallow(<TaskNew />);

    wrapper.find(Form.Input).at(0).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // $FlowExpectedError
    expect(wrapper.containsMatchingElement(<Form.Input value="Kaffepause" />)).toEqual(true);
/*
    wrapper.find(Form.Input).at(1).simulate('change', { currentTarget: { value: 'Drikepause' } });
    // $FlowExpectedError
    expect(wrapper.containsMatchingElement(<Form.Input value="Drikepause" />)).toEqual(true);
*/
    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/4');
      done();
    });
  });

})

describe("Del 2, sjekk TaskDetails", () => {
  test("TaskDetails draw correctly", (done) =>{
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);
/*
    setTimeout(() => {
      expect(
        wrapper.containsMatchingElement(
          "Title:", 'Les leksjon',"Description:", "One",
        )
      ).toEqual(true);
*/
setTimeout(() => {
    expect(wrapper).toMatchSnapshot();
    
    done();
  });
});
});

describe("Del 3, 75% av task-components.js", () => {
  test("TaskEdit draw correctly", (done) =>{
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);
  /*
    setTimeout(() => {
      expect(
        wrapper.containsMatchingElement(
          "Title:", 'Les leksjon',"Description:", "One",
        )
      ).toEqual(true);
  */
  setTimeout(() => {
    expect(wrapper).toMatchSnapshot();
  });

  wrapper.find(Form.Input).at(0).simulate('change', { currentTarget: { value: 'New title' } });
  // $FlowExpectedError
  expect(wrapper.containsMatchingElement(<Form.Input value="New title" />)).toEqual(true);

  wrapper.find(Form.Input).at(1).simulate('change', { currentTarget: { value: 'New description' } });
    // $FlowExpectedError
    expect(wrapper.containsMatchingElement(<Form.Input value="New description" />)).toEqual(true);

  wrapper.find(Button.Success).simulate('click');
  // Wait for events to complete
  setTimeout(() => {
    expect(location.hash).toEqual('#/tasks/1');

  wrapper.find(Button.Danger).simulate('click');
  // Wait for events to complete
  setTimeout(() => {
    expect(location.hash).toEqual('#/tasks/')
    done();
      });
    });
  });
});

