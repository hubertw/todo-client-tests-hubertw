// @flow

import * as React from 'react';
import { Alert } from '../src/widgets.js';
import { shallow } from 'enzyme';

describe('Alert tests', () => {
  test('No alerts initially', () => {
    const wrapper = shallow(<Alert />);

    expect(wrapper.matchesElement(<></>)).toEqual(true);
  });

  test('Show alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      done();
    });
  });

  test('Close alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      wrapper.find('button.close').simulate('click');

      expect(wrapper.matchesElement(<></>)).toEqual(true);

      done();
    });
  });

  test('Create 3 alerts, delete second and check if 1 and 3 are still there', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');
    Alert.success('test2');
    Alert.info('test3');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test<button>&times;</button>
            </div>
            <div>
              test2<button>&times;</button>
            </div>
            <div>
              test3<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      wrapper.find('button.close').at(1).simulate('click');

      expect(wrapper.matchesElement(<>
      <div>
        test<button>&times;</button>
      </div>
      <div>
        test3<button>&times;</button>
      </div></>)).toEqual(true);

      done();
    });
  });

});
